FROM php:7.2-fpm-alpine

RUN apk update \
    && apk add  --no-cache \
    git \
    curl \
    icu-dev \
    libxml2-dev \
    g++ \
    make \
    autoconf \
    mysql-client \
    python \
    yarn \
    freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev \
    && docker-php-source extract \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && docker-php-source delete \
    && docker-php-ext-install pdo_mysql soap intl zip sysvsem \
    && docker-php-ext-configure gd \
           --with-gd \
           --with-freetype-dir=/usr/include/ \
           --with-png-dir=/usr/include/ \
           --with-jpeg-dir=/usr/include/ && \
         NPROC=$(getconf _NPROCESSORS_ONLN) && \
         docker-php-ext-install -j${NPROC} gd && \
         apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /tmp/*

COPY . /usr/src/app

WORKDIR /usr/src/app

RUN composer install --no-progress --no-suggest --no-interaction \
    && yarn install \
    && yarn encore dev