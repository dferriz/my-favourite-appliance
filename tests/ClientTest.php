<?php

namespace App\Tests;

use App\Console\Service\DataImporter\Base\Cache\EndpointCache;
use App\Console\Service\DataImporter\Crawler\Client\Client;
use GuzzleHttp\Client as GuzzleClient;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientTest extends WebTestCase
{

    /**
     * @throws \ReflectionException
     */
    public function testSleepIfProceed()
    {
        $config = ['product_cache_dir'=>'var/cache/scrap/products', 'endpoint_cache_dir'=>'var/cache/scrap/endpoint'];
        $client = new Client(new EndpointCache($config), new GuzzleClient(), 0.5, 250000);

        // Testing implementing reflection
        $class = new ReflectionClass(Client::class);
        $method = $class->getMethod('sleepIfProceed');
        $method->setAccessible(true);

        // First client attemp, no sleep but it must sleep cause did the job in less than 0.5 secs
        $this->assertTrue(( $method->invoke($client) === false ));
        usleep(250000);
        // We sleeped
        $this->assertTrue(( $method->invoke($client) === true ));

    }
}
