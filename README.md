Requerimientos
============================
https://symfony.com/doc/current/reference/requirements.html
yarn o Docker >= v18.09.8

Especificaciones
============================
Desarrollar un catálogo online de productos obteniendo la información mediante web scraping.
En la web podran crearse usuarios y estos dispondran de una lista de productos deseados con la que podrán interactuar.
Esta lista podrá ser compartida públicamente aunque sólo el usuario propietario podrá modificarla.
Aplicar métodos que eviten saturar el servidor.
A la hora de desarrollar el importador dejarlo lo más abierto a una posible extensión (migración a importación mediante API, etc).

Instalación con Docker
============================

Ejecutar desde el raíz del proyecto los siguientes comandos:

    docker-compose up -d --build 
    docker exec -it my-favourite-appliance_php /bin/sh
    php bin/console doctrine:schema:update --force --complete
    php bin/console app:install
    

Instalación en local
=======================
1. Crear la base de datos my_favourite_appliance y el usuario mfa_user con password password.
2. Para modificar los datos de conexión a bbdd editar la siguiente línea en archivo .env:

    
    DATABASE_URL=mysql://db_user:db_passwd@127.0.0.1:3306/db_name
    
3. Ejecutar comandos.

    
    php bin/console doctrine:schema:update --force --complete 
    php bin/console app:install
    yarn install
    yarn encore dev
    php bin/console server start   
   
    
Comandos
====================
Instalación endpoints y usuarios por defecto

    php bin/console app:install
    
Sincronización de catálogo:

    
    php bin/console app:syncronize-catalog
    
Eliminar catálogo y archivos asociados:

    
    php bin/console app:clear-catalog


Frontend
================
Usuarios de prueba
------------------
    
    usuario: John Doe
    password: password
    
    usuario: alice
    password: password
     
Endpoints iniciales
-------------------
    https://www.appliancesdelivered.ie/dishwashers
    https://www.appliancesdelivered.ie/search/small-appliances
    
Tema
-------
- Se ha usado una plantilla HTML de Bootstrap gratuita y se ha adaptado como template de Twig.
- Se ha utilizado Encore Webpack para el empaquetamiento y generación de los assets.

Controladores
-------
- Main: Controla la vista de listado del catálogo.
- Product: Controla la vista de ficha de producto.
- Wishlist: Controla la vista de la lista de productos deseados.
- FOSUserBundle: Controla las vistas de registro y acceso de usuarios.

Backend
=============
Se ha creado una estructura base de importación de datos definida mediante interfaces con la intención
de facilitar la implementación de nuevos métodos de importación.
Esta estructura permite llamar a un servicio Job el cual tiene asignado un Worker que interactua con un Reader y un Storer. 
Obviamente no habría problema en inyectar a un Job tantos Workers necesite e ir iterandolos.

Scraping 
============
- Se ha utilizado XPath para leer el DOM de los endpoints.
- Se ha creado una clase Client mediante la cual podemos controlar el tiempo entre peticiones al servidor.



Endpoint
------------
- Se utiliza esta entidad para almacenar todos los endpoints a consultar, de momento del tipo Category y Product.
- Los endpoint tipo Category son los primeros que se consultan para obtener los endpoints de Product.
- En cada ciclo de importación del catálogo se marcan como "no chequeados" de forma que al terminar el ciclo se eliminen estos. Ocurre lo mismo con los productos.
- Se ha añadido un campo 'lastChecked' en el que mediante un timestamp se almacena la fecha de la última comprobación. 
Útil, por ejemplo, si se quiere hacer una comprobación de un endpoint de producto mediante ajax desde el frontend. 

 
Producto
----------
- El Id se obtiene de la url del mismo. Tanto el id en origen como en destino es el mismo por comodidad, lo lógico sería añadir un campo adicional "externalId".
- Las características y sus valores las almacenamos utilizando sus textos como identificadores.
- Comprobamos si el producto tiene stock mediante la existencia de la URL de la imagen on-stock.jpg en la ficha de producto.

Imagenes
---------
- Uso de Lyfecircle Events de Doctrine para la eliminación de ficheros asociados.
- Para identificar las imagenes se ha optado por el hasheo, mediante uuid5, de sus URL.


Productos deseados
----------
- Se ha añadido el ID del usuario en la URI de la wishlist cuando se comparte por simple comodidad, se podría hashear o tokenizar.
- Añadido la funcionalidad de que el usuario pueda elegir compartir o no su lista de seseados. 


Notas
------
- He creado una pseudo caché la cual almacena en fichero las respuestas de cada endpoint con motivo de evitar saturar 
el servidor fuente durante las pruebas. Eliminada quedaría en una entrega, se sobreentiende.