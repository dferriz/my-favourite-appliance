<?php
declare(strict_types=1);

namespace App\ValueObject;

/**
 * Class EndpointVO
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   1/08/19 23:00
 * @package App\ValueObject
 */
final class EndpointVO
{
    private $entity;
    private $entityId;
    private $endpoint;

    public function __construct($entity, $entityId, $endpoint) {
        $this->entity = $entity;
        $this->entityId = $entityId;
        $this->endpoint = $endpoint;
    }


    public function getEntity() :string {
        return $this->entity;
    }

    public function getEntityId() :int {
        return $this->entityId;
    }

    public function getEndpoint() :string {
        return $this->endpoint;
    }
}