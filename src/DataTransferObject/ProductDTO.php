<?php
declare(strict_types=1);

namespace App\DataTransferObject;

/**
 * Class ProductDTO
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   2/08/19 12:38
 * @package App\ValueObject
 */
class ProductDTO
{
    private $id;
    private $name;
    private $overview;
    private $price;
    private $pricePrevious;
    private $hasStock;
    private $features;
    private $images;

    public function __construct(int $id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }

    public function setOverview($overview) {
        $this->overview = $overview;
    }
    public function getOverview() {
        return $this->overview;
    }

    public function setPrice($price) {
        $this->price = $price;
    }
    public function getPrice() {
        return $this->price;
    }

    public function setHasStock(bool $hasStock) {
        $this->hasStock = $hasStock;
    }
    public function hasStock() :bool {
        return $this->hasStock;
    }

    public function setPricePrevious($pricePrevious) {
        $this->pricePrevious = $pricePrevious;
    }
    public function getPricePrevious() {
        return $this->pricePrevious;
    }

    public function setFeatures(array $features) {
        $this->features = $features;
    }
    public function getFeatures() {
        return $this->features;
    }

    public function setImages(array $images) {
        $this->images = $images;
    }
    public function getImages() {
        return $this->images;
    }

}