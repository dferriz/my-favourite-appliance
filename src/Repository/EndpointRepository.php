<?php

namespace App\Repository;

use App\Entity\Endpoint;
use App\Util\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Endpoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method Endpoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method Endpoint[]    findAll()
 * @method Endpoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EndpointRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Endpoint::class);
    }

    /**
     * @param int   $page
     * @param int   $pageSize
     * @param array $params
     * @param array $sort
     *
     * @return Paginator
     */
    public function findPaginatedBy(int $page, $params = [], $sort = [], int $pageSize = 12) {

        $qb = $this->createQueryBuilder('e');
        foreach( $params as $field=>$value ) {
            $clausule = vsprintf("e.%s = :%s", [$field, $field]);
            $qb->andWhere($clausule);
            $qb->setParameter($field, $value);
        }
        foreach( $sort as $field=>$order ){
            $clausule = vsprintf("e.%s", [$field]);
            $qb->addOrderBy($clausule, $order);
        }

        $paginator = new Paginator($qb, $pageSize);

        return $paginator->paginate($page);

    }

    public function uncheckAll() {

        $query = $this->createQueryBuilder('e')
            ->update()
            ->set('e.isChecked', ':isChecked')
            ->setParameter('isChecked', 0)->getQuery();

        return $query->execute();
    }

    /**
     * @return int
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeUnchecked() {

        $em = $this->getEntityManager();
        $removed = 0;
        do {
            /** @var Paginator $paginator */
            $paginator = $this->findPaginatedBy(1, ['isChecked'=>0]);

            $results = $paginator->getResults();
            foreach($results as $item) {
                /** @var Endpoint $item */
                $em->remove($item);
            }
            $em->flush();
            $em->clear();
            $removed++;

        } while ( $paginator->getNumResults() > 0 );

        return $removed;
    }

    // /**
    //  * @return Endpoint[] Returns an array of Endpoint objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Endpoint
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
