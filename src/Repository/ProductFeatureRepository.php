<?php

namespace App\Repository;

use App\Entity\ProductFeature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductFeature|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductFeature|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductFeature[]    findAll()
 * @method ProductFeature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductFeatureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductFeature::class);
    }


}
