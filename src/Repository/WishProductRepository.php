<?php

namespace App\Repository;

use App\Entity\WishProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WishProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method WishProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method WishProduct[]    findAll()
 * @method WishProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WishProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WishProduct::class);
    }

    // /**
    //  * @return WishProduct[] Returns an array of WishProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WishProduct
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
