<?php

namespace App\Repository;

use App\Entity\Product;
use App\Util\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param int    $page
     * @param string $field
     * @param string $order
     *
     * @return Paginator
     */
    public function findAllPaginated(int $page, $field = 'price', $order = 'ASC') {

        return $this->findPaginatedBy($page, [], [$field=>$order]);
    }

    /**
     * @param int   $page
     * @param       $params
     * @param array $sort
     *
     * @return Paginator
     */
    public function findPaginatedBy(int $page, $params, $sort = []) {

        $qb = $this->createQueryBuilder('p');
        foreach( $params as $field=>$value ) {
            $clausule = vsprintf("p.%s = :%s", [$field, $field]);
            $qb->andWhere($clausule);
            $qb->setParameter($field, $value);
        }
        foreach( $sort as $field=>$order ){
            $clausule = vsprintf("p.%s", [$field]);
            $qb->addOrderBy($clausule, $order);
        }

        $paginator = new Paginator($qb);

        return $paginator->paginate($page);

    }

    public function uncheckAll() {

        $query = $this->createQueryBuilder('p')
            ->update()
            ->set('p.isChecked', ':isChecked')
            ->setParameter('isChecked', 0)->getQuery();

        return $query->execute();
    }

    /**
     * @return int
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeUnchecked() {

        $em = $this->getEntityManager();
        $removed = 0;
        do {
            /** @var Paginator $paginator */
            $paginator = $this->findPaginatedBy(1, ['isChecked'=>0]);

            $results = $paginator->getResults();
            foreach($results as $product) {
                /** @var Product $product */
                $em->remove($product);
                $removed++;
            }
            $em->flush();
            $em->clear();

        } while ( $paginator->getNumResults() > 0 );

        return $removed;
    }


    public function removeAll(){

        $em = $this->getEntityManager();
        do {

            /** @var Paginator $paginator */
            $paginator = $this->findAllPaginated(1);

            $results = $paginator->getResults();
            foreach($results as $product) {
                $em->remove($product);
            }
            $em->flush();
            $em->clear();
        } while( $paginator->getNumResults() > 0 );

    }



    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
