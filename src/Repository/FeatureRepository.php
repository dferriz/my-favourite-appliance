<?php

namespace App\Repository;

use App\Entity\Feature;
use App\Util\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Feature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feature[]    findAll()
 * @method Feature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeatureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Feature::class);
    }


    public function findPaginatedBy(int $page, $params = [], $sort = []) {

        $qb = $this->createQueryBuilder('f');
        foreach( $params as $field=>$value ) {
            $clausule = vsprintf("f.%s = :%s", [$field, $field]);
            $qb->andWhere($clausule);
            $qb->setParameter($field, $value);
        }
        foreach( $sort as $field=>$order ){
            $clausule = vsprintf("f.%s = :%s", [$field, $order]);
            $qb->addOrderBy($clausule);
        }

        $paginator = new Paginator($qb);

        return $paginator->paginate($page);

    }

    public function removeAll(){

        $em = $this->getEntityManager();
        do {

            /** @var Paginator $paginator */
            $paginator = $this->findPaginatedBy(1);

            $results = $paginator->getResults();
            foreach($results as $entity) {
                $em->remove($entity);
            }
            $em->flush();
            $em->clear();
        } while( $paginator->getNumResults() > 0 );

    }
    // /**
    //  * @return Feature[] Returns an array of Feature objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Feature
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
