<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WishProduct", mappedBy="user", orphanRemoval=true)
     */
    private $wishProducts;

    /**
     * @ORM\Column(type="boolean")
     */
    private $shareWishlist;

    public function __construct() {
        parent::__construct();
        $this->wishProducts = new ArrayCollection();
    }

    /**
     * @return Collection|WishProduct[]
     */
    public function getWishProducts(): Collection
    {
        return $this->wishProducts;
    }

    public function addWishProduct(WishProduct $wishProduct): self
    {
        if (!$this->wishProducts->contains($wishProduct)) {
            $this->wishProducts[] = $wishProduct;
            $wishProduct->setUser($this);
        }

        return $this;
    }

    public function removeWishProduct(WishProduct $wishProduct): self
    {
        if ($this->wishProducts->contains($wishProduct)) {
            $this->wishProducts->removeElement($wishProduct);
            // set the owning side to null (unless already changed)
            if ($wishProduct->getUser() === $this) {
                $wishProduct->setUser(null);
            }
        }

        return $this;
    }

    public function isWished(Product $product) {
        foreach( $this->wishProducts->getIterator() as $i=>$item) {
            /** @var Product $wishedProduct */
            $wishedProduct = $item->getProduct();
            if ( $product->getId() === $wishedProduct->getId())
                return true;

        }
        return false;
    }

    public function getShareWishlist(): ?bool
    {
        return $this->shareWishlist;
    }

    public function setShareWishlist(bool $shareWishlist): self
    {
        $this->shareWishlist = $shareWishlist;

        return $this;
    }
}
