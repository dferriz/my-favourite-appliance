<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $overview;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=2, nullable=true)
     */
    private $pricePrevious;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasStock;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductFeature", mappedBy="product", orphanRemoval=true)
     */
    private $productFeatures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductImage", mappedBy="product", orphanRemoval=true)
     */
    private $productImages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WishProduct", mappedBy="product", orphanRemoval=true)
     */
    private $wishProduct;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isChecked = true;


    public function __construct(int $id)
    {
        $this->id = $id;
        $this->reviews = new ArrayCollection();
        $this->productFeatures = new ArrayCollection();
        $this->productImages = new ArrayCollection();
        $this->productCategory = new ArrayCollection();
        $this->wishProduct = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOverview()
    {
        return $this->overview;
    }

    public function setOverview($overview): self
    {
        $this->overview = $overview;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPricePrevious()
    {
        return $this->pricePrevious;
    }

    public function setPricePrevious($pricePrevious): self
    {
        $this->pricePrevious = $pricePrevious;

        return $this;
    }

    public function getHasStock(): ?bool
    {
        return $this->hasStock;
    }

    public function setHasStock(bool $hasStock): self
    {
        $this->hasStock = $hasStock;

        return $this;
    }


    /**
     * @return Collection|ProductFeature[]
     */

    public function getProductFeatures(): Collection
    {
        return $this->productFeatures;
    }

    public function addProductFeature(ProductFeature $productFeature): self
    {
        if (!$this->productFeatures->contains($productFeature)) {
            $this->productFeatures[] = $productFeature;
            $productFeature->addProduct($this);
        }

        return $this;
    }

    public function removeProductFeature(ProductFeature $productFeature): self
    {
        if ($this->productFeatures->contains($productFeature)) {
            $this->productFeatures->removeElement($productFeature);
            $productFeature->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getProductImages(): Collection
    {
        return $this->productImages;
    }

    public function addProductImage(ProductImage $productImage): self
    {
        if (!$this->productImages->contains($productImage)) {
            $this->productImages[] = $productImage;
            $productImage->setProduct($this);
        }

        return $this;
    }

    public function removeProductImage(ProductImage $productImage): self
    {
        if ($this->productImages->contains($productImage)) {
            $this->productImages->removeElement($productImage);
            // set the owning side to null (unless already changed)
            if ($productImage->getProduct() === $this) {
                $productImage->setProduct(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|WishProduct[]
     */
    public function getWishProduct(): Collection
    {
        return $this->wishProduct;
    }

    public function addWishProduct(WishProduct $wishProduct): self
    {
        if (!$this->wishProduct->contains($wishProduct)) {
            $this->wishProduct[] = $wishProduct;
            $wishProduct->setProduct($this);
        }

        return $this;
    }

    public function removeWishProduct(WishProduct $wishProduct): self
    {
        if ($this->wishProduct->contains($wishProduct)) {
            $this->wishProduct->removeElement($wishProduct);
            // set the owning side to null (unless already changed)
            if ($wishProduct->getProduct() === $this) {
                $wishProduct->setProduct(null);
            }
        }

        return $this;
    }

    public function getIsChecked(): ?bool
    {
        return $this->isChecked;
    }

    public function setIsChecked(bool $isChecked): self
    {
        $this->isChecked = $isChecked;

        return $this;
    }

}
