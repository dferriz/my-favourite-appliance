<?php
declare(strict_types=1);

namespace App\Http\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ProductTwigExtension
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   3/08/19 14:18
 * @package App\Http\Twig
 */
class ProductTwigExtension extends AbstractExtension
{
    private $container;

    public function __construct(ContainerInterface $container) {

        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getImageUrl', [$this, 'getImageUrl']),
            new TwigFunction('getBlobStream', [$this, 'getBlobStream'])
        ];
    }

    public function getImageUrl($productId, $fileName) {

        $rootDir= $this->container->getParameter('product_image_web_dir');
        $filePath = vsprintf("%s/%s/%s", [$rootDir, implode('/', str_split(strval($productId))), $fileName]);

        return $filePath;
    }

    public function getBlobStream($blob) {
        return stream_get_contents($blob);
    }
}