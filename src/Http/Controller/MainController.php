<?php

namespace App\Http\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class MainController
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   3/08/19 0:40
 * @package App\Http\Controller
 *
 * @Route("/")
 */
class MainController extends AbstractController
{
    /**
     * Shows a paginated product list view.
     * @Route("/", defaults={"page": "1"}, methods={"GET"},  name="main_index")
     * @Route("/page/{page<[1-9]\d*>}", methods={"GET"}, name="main_index_paginated")
     *
     * @param Request           $request
     * @param                   $page
     * @param ProductRepository $productRepository
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, $page, ProductRepository $productRepository)
    {
        $orderByOptions = [
            'price' => ['field'=>'price', 'order' => 'asc'],
            '!price' => ['field' => 'price', 'order' => 'desc'],
            'title' => ['field'=>'name', 'order'=>'asc'],
            '!title' => ['field'=>'name', 'order'=>'desc']
        ];

        $orderByKey = $request->get('order');

        if ( !$orderByKey || !array_key_exists($orderByKey, $orderByOptions) ) {
            $orderByKey = 'price';
        }

        $orderBy = $orderByOptions[$orderByKey];
        $paginator = $productRepository->findAllPaginated($page, $orderBy['field'], $orderBy['order']);


        return $this->render('home/index.html.twig', [
            'paginator' => $paginator
        ]);
    }

}
