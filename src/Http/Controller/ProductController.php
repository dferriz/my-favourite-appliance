<?php

namespace App\Http\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   3/08/19 16:26
 * @package App\Http\Controller
 *
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * Shows the product view.
     *
     * @Route("/{product}", name="product_index")
     *
     * @param Product $product
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Product $product)
    {
        return $this->render('product/index.html.twig', [
            'product' => $product
        ]);
    }
}
