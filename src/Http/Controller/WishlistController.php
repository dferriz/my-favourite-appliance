<?php

namespace App\Http\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Entity\WishProduct;
use App\Repository\WishProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class WishlistController
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   3/08/19 18:05
 * @package App\Http\Controller
 *
 * @Route("/wishlist")
 */
class WishlistController extends AbstractController
{
    /**
     * Shows the user's product wishlist view.
     *
     * @Route("/", name="wishlist_index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(){

        /** @var User $user */
        $user = $this->getUser();
        if (!$user instanceof User)
            return $this->redirectToRoute('main_index');
        $wishProducts = $user->getWishProducts();

        return $this->render('wishlist/index.html.twig', [
            'wishProducts' => $wishProducts->getIterator()
        ]);
    }

    /**
     * Shows the public user's product wishlist view.
     *
     * @Route("/show/{user}", name="wishlist_show_friend")
     *
     * @param User $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showFriend(User $user) {

        if ( !$user->getShareWishlist() ) {
            return $this->redirectToRoute('main_index');
        }
        $wishProducts = $user->getWishProducts();

        return $this->render('wishlist/index.html.twig', [
            'wishProducts' => $wishProducts->getIterator()
        ]);
    }

    /**
     * Adds a Product to user's wishlist.
     *
     * @Route("/{product_id}/add", name="wishlist_product_add")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @ParamConverter("product", options={"mapping": {"product_id": "id"}})
     *
     * @param Product               $product
     * @param WishProductRepository $wishProductRepository
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function productAdd(Product $product, WishProductRepository $wishProductRepository) {

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $wishProduct = $wishProductRepository->findOneBy(['user'=>$user->getId(), 'product'=>$product->getId()]);
        if (!$wishProduct instanceof WishProduct) {
            $wishProduct = new WishProduct();
            $wishProduct->setUser($user);
            $wishProduct->setProduct($product);
            $em->persist($wishProduct);
            $em->flush();
            $em->clear();
            $this->addFlash('success', 'Product successfully added to your wishlist.');
        } else {
            // Already pushed
            $this->addFlash('warning', 'You have already added this product to your wishlist.');
        }

        return $this->redirectToRoute('wishlist_index');
    }

    /**
     * Remove a Product to user's wishlist.
     *
     * @Route("/{product_id}/remove", name="wishlist_product_remove")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @ParamConverter("product", options={"mapping": {"product_id": "id"}})
     *
     * @param Product               $product
     * @param WishProductRepository $wishProductRepository
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function productRemove(Product $product, WishProductRepository $wishProductRepository) {

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $wishProduct = $wishProductRepository->findOneBy(['user'=>$user->getId(), 'product'=>$product->getId()]);
        if ($wishProduct instanceof WishProduct) {
            $em->remove($wishProduct);
            $em->flush();
            $em->clear();
            $this->addFlash('success', 'Product successfully removed from your wishlist.');
        } else {
            // Already pushed
            $this->addFlash('warning', 'This product is not in your wishlist.');
        }

        return $this->redirectToRoute('wishlist_index');
    }


    /**
     * Toggles public access to user's wishlist.
     *
     * @Route("/share/toggle", name="wishlist_share")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toggleShareWishlist() {

        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $user->setShareWishlist(!$user->getShareWishlist());
        $em->persist($user);
        $em->flush();
        $em->clear();

        return $this->redirectToRoute('wishlist_index');
    }
}
