<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Entity\ProductImage;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ProductImageEventListener
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   2/08/19 23:37
 * @package App\EventListener
 */
class ProductImageEventListener
{
    protected $container;
    private $filesystem;

    public function __construct(ContainerInterface $container, Filesystem $filesystem) {
        $this->container = $container;
        $this->filesystem = $filesystem;
    }

    public function postRemove(LifecycleEventArgs $args) {

        $entity = $args->getObject();

        /**
         * Removes image file.
         */
        if ( $entity instanceof ProductImage ) {
            $productId = $entity->getProduct()->getId();
            $fileName = $entity->getFileName();
            $rootDir= $this->container->getParameter('product_image_root_dir');
            $filePath = vsprintf("%s/%s/%s", [$rootDir, implode('/', str_split(strval($productId))), $fileName]);

            /** @var Filesystem $fs */
            if ( $this->filesystem->exists($filePath) ) {
                $this->filesystem->remove($filePath);
            }

        }
    }
}