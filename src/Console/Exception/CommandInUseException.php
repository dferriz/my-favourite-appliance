<?php
declare(strict_types=1);

namespace App\Console\Exception;


use Throwable;

/**
 * Class CommandInUseException
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   5/08/19 18:34
 * @package App\Console\Exception
 */
class CommandInUseException extends \RuntimeException
{
    public function __construct( int $code = 0, Throwable $previous = null) {

        parent::__construct("Sorry, this command is being used by another process.", $code, $previous);
    }

}