<?php
declare(strict_types=1);

namespace App\Console\Service\DataImporter\Base\Job;

/**
 * Interface JobInterface
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   31/07/19 0:02
 * @package App\Console\Service\DataImporter\Base\Job
 */
interface JobInterface
{
    public function doJob();
}