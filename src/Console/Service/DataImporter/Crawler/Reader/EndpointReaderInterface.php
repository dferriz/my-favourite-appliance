<?php
declare(strict_types=1);

namespace App\Console\Service\DataImporter\Crawler\Reader;

use App\Console\Service\DataImporter\Base\Reader\EndpointReaderInterface as BaseInterface;
/**
 * Interface EndpointReaderInterface
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   9/08/19 0:15
 * @package App\Console\Service\DataImporter\Crawler\Reader
 */
interface EndpointReaderInterface extends BaseInterface
{

}