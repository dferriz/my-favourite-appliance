<?php
declare(strict_types=1);

namespace App\Console\Service\DataImporter\Crawler\Worker;

use App\Console\Service\DataImporter\Base\Worker\ProductWorkerInterface as BaseInterface;
/**
 * Interface ProductWorkerInterface
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   9/08/19 0:12
 * @package App\Console\Service\DataImporter\Crawler\Worker
 */
interface ProductWorkerInterface extends BaseInterface
{

}