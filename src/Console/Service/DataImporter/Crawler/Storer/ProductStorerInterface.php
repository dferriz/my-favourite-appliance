<?php
declare(strict_types=1);

namespace App\Console\Service\DataImporter\Crawler\Storer;

use App\Console\Service\DataImporter\Base\Storer\ProductStorerInterface as BaseInterface;

/**
 * Interface ProductStorerInterface
 *
 * @author  David Ferriz Candela <dev@dferriz.es>
 * @since   9/08/19 0:10
 * @package App\Console\Service\DataImporter\Crawler\Storer
 */
interface ProductStorerInterface extends BaseInterface
{

}