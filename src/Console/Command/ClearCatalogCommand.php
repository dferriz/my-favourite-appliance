<?php

namespace App\Console\Command;

use App\Entity\Feature;
use App\Entity\Product;
use App\Repository\FeatureRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;

class ClearCatalogCommand extends Command
{
    protected static $defaultName = 'app:clear-catalog';

    private $entityManager;
    private $style;

    public function __construct(
        EntityManagerInterface $entityManager,
        StyleInterface $style
    )
    {
        $this->entityManager = $entityManager;
        $this->style = $style;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Cleans catalog data')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->style->title("WELCOME TO CATALOG CLEAN UP COMMAND!");

        $this->style->text("Removing <info>Product</info> entities");
        /** @var ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);
        $productRepository->removeAll();

        $this->style->text("Removing <info>Feature</info> entities");
        /** @var FeatureRepository $featureRepository */
        $featureRepository = $this->entityManager->getRepository(Feature::class);
        $featureRepository->removeAll();

        $this->style->success("Task completed!");

    }
}
