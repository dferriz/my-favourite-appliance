<?php

namespace App\Console\Command;

use App\Entity\Endpoint;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;

class InstallCommand extends Command
{
    protected static $defaultName = 'app:install';

    private $entityManager;
    private $style;

    public function __construct(
        EntityManagerInterface $entityManager,
        StyleInterface $style
    ) {
        $this->entityManager = $entityManager;
        $this->style = $style;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription("Install default data to start to work.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $endpointRepository = $this->entityManager->getRepository(Endpoint::class);

        $endpoints = [
            1 => 'https://www.appliancesdelivered.ie/dishwashers',
            2 => 'https://www.appliancesdelivered.ie/search/small-appliances'
        ];

        foreach($endpoints as $id => $endpointLink) {
            $this->style->text(vsprintf("Adding %s endpoint -> %s ", [Endpoint::ENTITY_CATEGORY, $endpointLink]));
            $endpoint = $endpointRepository->findOneBy(['entityId'=>$id, 'entity'=>Endpoint::ENTITY_CATEGORY]);
            if (!$endpoint instanceof Endpoint){
                $endpoint = new Endpoint();
                $endpoint->setEntity(Endpoint::ENTITY_CATEGORY);
                $endpoint->setEntityId($id);
            }

            $endpoint->setEndpoint($endpointLink);
            $endpoint->setLastCheck(time());
            $endpoint->setIsChecked(true);
            $this->entityManager->persist($endpoint);
            $this->entityManager->flush();
        }

        $users = [
            ['username'=>'John Doe', 'usernameCanonical'=>'john doe', 'email'=>'example@example.com', 'password'=>'$2y$13$KyHC6KdIxYCM6Z0NjbTh5.0L41VSTxAdlOYlTQ0N49Tei.Ain5y1u'],
            ['username'=>'Alice', 'usernameCanonical'=>'alice', 'email'=>'alice@example.com', 'password'=>'$2y$13$KyHC6KdIxYCM6Z0NjbTh5.0L41VSTxAdlOYlTQ0N49Tei.Ain5y1u']
        ];

        $userRepository = $this->entityManager->getRepository(User::class);

        foreach($users as $usr) {

            $user = $userRepository->findOneBy(['email'=>$usr['email'], 'usernameCanonical'=>$usr['usernameCanonical']]);
            if (!$user instanceof User){
                $user = new User();
                $user->setUsername($usr['username']);
                $user->setUsernameCanonical($usr['usernameCanonical']);
                $user->setEmail($usr['email']);
                $user->setEmailCanonical($usr['email']);
            }
            $user->setPassword($usr['password']); // "password"
            $user->setEnabled(true);
            $user->setShareWishlist(false);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->entityManager->clear();
        }

        $this->style->success("Installation completed!");


    }
}
