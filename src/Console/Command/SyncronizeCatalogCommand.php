<?php
declare(strict_types=1);

namespace App\Console\Command;


use App\Console\Exception\CommandInUseException;
use App\Console\Service\DataImporter\Crawler\Job\ImportCatalogJobInterface;
use App\Util\Timer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Lock\Factory;
use Symfony\Component\Lock\Store\SemaphoreStore;

/**
 * Class SyncronizeCatalogCommand
 *
 * @author David Ferriz Candela <dev@dferriz.es>
 * @since  30/07/19 23:12
 */
class SyncronizeCatalogCommand extends Command
{
    protected static $defaultName = 'app:syncronize-catalog';

    private $entityManager;
    private $importCatalogJob;
    private $logger;
    private $style;
    private $lock;


    public function __construct(
        EntityManagerInterface $entityManager,
        ImportCatalogJobInterface $importCatalogJob,
        LoggerInterface $logger,
        StyleInterface $style
    ) {
        $this->entityManager = $entityManager;
        $this->importCatalogJob = $importCatalogJob;
        $this->logger = $logger;
        $this->style = $style;

        $store = new SemaphoreStore();
        $factory = new Factory($store);
        $this->lock = $factory->createLock('syncronize-catalog');

        parent::__construct();
    }


    protected function configure()
    {
        $this->setDescription("Syncronices catalog");
    }


    /**
     * Tries to execute the command. It uses PHP Semaphores.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void|CommandInUseException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock->acquire()){
            throw new CommandInUseException();
        }
        $this->importCatalogJob->doJob();
        $this->printCommandStats();
        $this->lock->release();
    }

    protected function printCommandStats() {

        $response = new JsonResponse( Timer::getInstance()->results(true) );
        $response->setEncodingOptions( $response->getEncodingOptions() | JSON_PRETTY_PRINT );
        $this->logger->debug($response->getContent());
    }
}